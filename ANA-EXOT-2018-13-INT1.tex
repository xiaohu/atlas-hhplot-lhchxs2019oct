%-------------------------------------------------------------------------------
% This file provides a skeleton ATLAS note.
% \pdfinclusioncopyfonts=1
% This command may be needed in order to get \ell in PDF plots to appear. Found in
% https://tex.stackexchange.com/questions/322010/pdflatex-glyph-undefined-symbols-disappear-from-included-pdf
%-------------------------------------------------------------------------------
% Specify where ATLAS LaTeX style files can be found.
\newcommand*{\ATLASLATEXPATH}{latex/}
% Use this variant if the files are in a central location, e.g. $HOME/texmf.
% \newcommand*{\ATLASLATEXPATH}{}
%-------------------------------------------------------------------------------
\documentclass[PUB, atlasdraft=false, texlive=2016, UKenglish]{\ATLASLATEXPATH atlasdoc}
% The language of the document must be set: usually UKenglish or USenglish.
% british and american also work!
% Commonly used options:
%  atlasdraft=true|false This document is an ATLAS draft.
%  texlive=YYYY          Specify TeX Live version (2016 is default).
%  coverpage             Create ATLAS draft cover page for collaboration circulation.
%                        See atlas-draft-cover.tex for a list of variables that should be defined.
%  cernpreprint          Create front page for a CERN preprint.
%                        See atlas-preprint-cover.tex for a list of variables that should be defined.
%  NOTE                  The document is an ATLAS note (draft).
%  PAPER                 The document is an ATLAS paper (draft).
%  CONF                  The document is a CONF note (draft).
%  PUB                   The document is a PUB note (draft).
%  BOOK                  The document is of book form, like an LOI or TDR (draft)
%  txfonts=true|false    Use txfonts rather than the default newtx
%  paper=a4|letter       Set paper size to A4 (default) or letter.

%-------------------------------------------------------------------------------
% Extra packages:
\usepackage{\ATLASLATEXPATH atlaspackage}
% Commonly used options:
%  biblatex=true|false   Use biblatex (default) or bibtex for the bibliography.
%  backend=bibtex        Use the bibtex backend rather than biber.
%  subfigure|subfig|subcaption  to use one of these packages for figures in figures.
%  minimal               Minimal set of packages.
%  default               Standard set of packages.
%  full                  Full set of packages.
%-------------------------------------------------------------------------------
% Style file with biblatex options for ATLAS documents.
\usepackage{\ATLASLATEXPATH atlasbiblatex}

% Package for creating list of authors and contributors to the analysis.
\usepackage{\ATLASLATEXPATH atlascontribute}

% Useful macros
\usepackage{\ATLASLATEXPATH atlasphysics}
% See doc/atlas_physics.pdf for a list of the defined symbols.
% Default options are:
%   true:  journal, misc, particle, unit, xref
%   false: BSM, heppparticle, hepprocess, hion, jetetmiss, math, process, other, texmf
% See the package for details on the options.

% Files with references for use with biblatex.
% Note that biber gives an error if it finds empty bib files.
% \addbibresource{ANA-EXOT-2018-13-INT1.bib}
\addbibresource{bib/ATLAS.bib}
\addbibresource{bib/CMS.bib}
\addbibresource{bib/ConfNotes.bib}
\addbibresource{bib/PubNotes.bib}

% Paths for figures - do not forget the / at the end of the directory name.
\graphicspath{{logos/}{figures/}}

% Add you own definitions here (file ANA-EXOT-2018-13-INT1-defs.sty).
\usepackage{ANA-EXOT-2018-13-INT1-defs}

%-------------------------------------------------------------------------------
% Generic document information
%-------------------------------------------------------------------------------

% Title, abstract and document
\input{ANA-EXOT-2018-13-INT1-metadata}
% Author and title for the PDF file
\hypersetup{pdftitle={ATLAS document},pdfauthor={The ATLAS Collaboration}}

%-------------------------------------------------------------------------------
% Content
%-------------------------------------------------------------------------------
\begin{document}

\maketitle

%\tableofcontents

% List of contributors - print here or after the Bibliography.
%\PrintAtlasContribute{0.30}

%-------------------------------------------------------------------------------
%\section{All plots to present}
%\label{sec:one}
%-------------------------------------------------------------------------------

%\clearpage
%\newpage

\begin{figure}
  {\centering
    \includegraphics[width=0.48\textwidth,page=15]{figure/HH_FT-lambda01_FTapprox-lambda01.pdf}
  \caption{The $m_{\textrm{HH}}$ distribution of the non-resonant gluon-fusion HH production,
  comparing Powheg-Box-V2 NLO+FT and NLO+FTApprox, both with the Higgs self-coupling $\kappa_\lambda=1$.
  FT stands for the full top-quark mass calculation up to NLO, which is going to be used
  in the full Run 2 analyses. FTApprox applies the full top-quark mass in the real part
  of the NLO correction, but uses infinite top-quark mass in the virtual part.
  The histograms are normalised to the same area, and only shapes should be compared.
  The ratio of FTApprox over FT is shown in the bottom pane.
  The statistical error is shown in bars and hatched bands in the bottom pane.}
  }
\end{figure}


\begin{figure}
  {\centering
    \includegraphics[width=0.48\textwidth,page=15]{figure/HH_lambda-05-PowhegFT-MG5LO.pdf}
    \includegraphics[width=0.48\textwidth,page=15]{figure/HH_lambda10-PowhegFT-MG5LO.pdf} \\
    \includegraphics[width=0.48\textwidth,page=15]{figure/HH_lambda02d5-PowhegFT-MG5LO.pdf}
  \caption{The $m_{\textrm{HH}}$ distribution of the non-resonant gluon-fusion HH production,
  comparing Powheg-Box-V2 NLO+FT and MG5\_aMC@NLO LO+FT, with the Higgs self-coupling $\kappa_\lambda=-5$, 2.5 and 10.
  Powheg-Box-V2 NLO will replace MG5\_aMC@NLO LO for the full Run 2 analyses.
  Typical $\kappa_\lambda$ values of -5 and 10, representing roughly
  the 95\% CL constraints from early Run 2 analyses, and of 2.5, sitting close to
  the maximal interference of the two leading diagrams of the gluon-fusion production, are shown.
  The histograms are normalised to the same area, and only shapes should be compared.
  The ratios of MG5\_aMC@NLO LO over Powheg-Box-V2 NLO+FT are shown in the bottom panes.
  The statistical error is shown in bars and hatched bands in the bottom panes.}
  }
\end{figure}

\begin{figure}
  {\centering
    \includegraphics[width=0.48\textwidth,page=15]{figure/HH_lambda10-PowhegFT-linearcomb-0_1_20.pdf}
    \includegraphics[width=0.48\textwidth,page=15]{figure/HH_lambda02d5-PowhegFT-linearcomb-0_1_20.pdf}
  \caption{The $m_{\textrm{HH}}$ distribution of the non-resonant gluon-fusion HH production,
  comparing Powheg-Box-V2 NLO+FT and the linear combination (LC) method,
  with the Higgs self-coupling $\kappa_\lambda=$ 2.5 and 10.
  As known, the differential cross-section of the gluon-fusion HH production can be
  written as a function of $\kappa_{\textrm{t}}$ (the top Yukawa coupling) and $\kappa_\lambda$.
  A formula can thus be derived to allow combining three MC samples with chosen $\kappa_\lambda$ values
  into a MC sample with a target $\kappa_\lambda$ value:
  $|A(k_t,k_{\lambda})|^2=k_t^{2} \bigg[ \Big(k_t^{2}+\frac{k_{\lambda}^{2}}{20}-
  \frac{399}{380}k_tk_{\lambda}\Big)|A(1,0)|^2+\Big(\frac{40}{38}k_tk_{\lambda}-
  \frac{2}{38}k_{\lambda}^{2}\Big)|A(1,1)|^2+
  \frac{k_{\lambda}^{2}-k_tk_{\lambda}}{380}|A(1,20)|^2\,$,
  where $A(k_t,k_{\lambda})$ stands for the amplitude and $|A(k_t,k_{\lambda})|^2$ can be regarded as MC samples.
  The LC method applies this formula using basis samples with $\kappa_\lambda=$ 0, 1, and 20.
  A closure, by comparing to the actual generated samples using Powheg-Box-V2 NLO+FT,
  is found in general, except for $\kappa_\lambda$ close to the maximal interference of
  the two leading diagrams of the gluon-fusion production.
  The histograms are normalised to the same area, and only shapes should be compared.
  The ratios of LC over Powheg-Box-V2 NLO+FT are shown in the bottom panes.
  The statistical error is shown in bars and hatched bands in the bottom panes.
  }
  }
\end{figure}

\begin{figure}
  {\centering
    \includegraphics[width=0.48\textwidth,page=48]{figure/HH_lambdap01-PowhegFT-HW7-PY8.pdf}
  \caption{The $m_{\textrm{HH}}$ distribution of the non-resonant gluon-fusion HH production,
  comparing Herwig7 and Pythia8, both connected to Powheg-Box-V2 NLO+FT.
  The histograms are normalised to the same area, and only shapes should be compared.
  The ratio of Pythia8 over Herwig7 is shown in the bottom pane.
  The statistical error is shown in bars and hatched bands in the bottom pane.}
  }
\end{figure}

\begin{figure}
  {\centering
    \includegraphics[width=0.48\textwidth]{figure/ggF_VBF_compare_mVBFjj.pdf}
    \includegraphics[width=0.48\textwidth]{figure/ggF_VBF_compare_VBFjjdEta.pdf}
  \caption{The distributions of $m_{\textrm{jj}}$ (VBF-like jets) and $m_{\textrm{HH}}$,
  comparing the VBF HH production that is generated with MG5\_aMC@NLO at LO interfaced to Herwig7
  and the gluon-fusion HH production that is generated with MG5\_aMC@NLO with NLO+FTApprox interfaced to Herwig7.
  The two VBF-like jets are required to have $|\eta|>2.0$,
  $p_{\textrm{T}}>30$ GeV and opposite signs of $\eta$ to each other;
  these have accepted cross-section of 2.23 fb for ggF and 0.61 fb for VBF.
  The histograms are normalised to the same area, and only shapes should be compared.
  }
  }
\end{figure}


\begin{figure}
  {\centering
    \includegraphics[width=0.48\textwidth]{figure/allhad_h_dR_WW.pdf}
  \caption{The distribution of $\Delta R(\textrm{WW})$ of $X\to SH$ model,
  where $S$ decays to $WW$ and both $W$ bosons decay hadronically,
  generated with Pythia8.
  The larger the mass difference between $X$ and $S$ particles,
  the larger the boost of $S$, and thus $\Delta R(\textrm{WW})$
  tends to be smaller resulting in merged topologies.}
  }
\end{figure}

%\begin{figure}
%  {\centering
%    \includegraphics[width=0.48\textwidth]{figure/}
%    \includegraphics[width=0.48\textwidth]{figure/}
%  \caption{}
%  }
%\end{figure}


%-------------------------------------------------------------------------------
% If you use biblatex and either biber or bibtex to process the bibliography
% just say \printbibliography here
%\printbibliography
% If you want to use the traditional BibTeX you need to use the syntax below.
%\bibliographystyle{bib/bst/atlasBibStyleWithTitle}
%\bibliography{ANA-EXOT-2018-13-INT1,bib/ATLAS,bib/CMS,bib/ConfNotes,bib/PubNotes}
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Print the list of contributors to the analysis
% The argument gives the fraction of the text width used for the names
%-------------------------------------------------------------------------------
%\clearpage
%The supporting notes for the analysis should also contain a list of contributors.
%This information should usually be included in \texttt{mydocument-metadata.tex}.
%The list should be printed either here or before the Table of Contents.
%\PrintAtlasContribute{0.30}


%-------------------------------------------------------------------------------
%\clearpage
%\appendix
%\part*{Appendices}
%\addcontentsline{toc}{part}{Appendices}
%-------------------------------------------------------------------------------

%In an ATLAS note, use the appendices to include all the technical details of your work
%that are relevant for the ATLAS Collaboration only (e.g.\ dataset details, software release used).
%This information should be printed after the Bibliography.

\end{document}
